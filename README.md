# Carter Perkins carterp@uoregon.edu
## Description
This is a Python flask webserver implementing PyMongo, JQuery, Javascript, and AJAX to create an ACP Brevet Control Times Calculator like the one described on www.rusa.org. This server checks implements AJAX in the calculator to get the opening and closing time for the brevets in real time. JQuery and JSON are used to transfer data between the server and the client's webpage, while Python is used for the server itself as well as the underlying logic for the time calculation. PyMongo is used to store brevets in the the database, which are fetched when displaying the results. Also included is a 'Clear' button for dropping the database manually, otherwise when 'Display' is pressed it will retrieve the last submitted contents from the database. The project utilizes token based authentication and hashed passwords before accessing API resources, which can be accessed using the below URL. Note that both the registration and log-in/token-fetch pages have "Log Out" buttons that will kill any active tokens. Registering a user does not log them in, and tokens are required before accessing any of the `localhost:5001` APIs. 



## Note on cURL commands
Wasn't able to get the cURL commands to work properly, however when using the program through the GUI the functionality described works properly.



## Running the Server
`docker-compose build`
`docker-compose up`
## The ACP Brevet Control Calculator
`localhost:5000` 
## Registering an Account
`localhost:5000/api/register`
## Logging In and Retrieving a Token
`localhost:5000/api/token`
## Accessing the API
`localhost:5001/listAll`
`localhost:5001/listOpenOnly`
`localhost:5001/listCloseOnly`
`localhost:5001/listAll/json`
`localhost:5001/listAll/csv`
`localhost:5001/listOpenOnly/json`
`localhost:5001/listOpenOnly/csv`
`localhost:5001/listCloseOnly/json`
`localhost:5001/listCloseOnly/csv`
`localhost:5001/listAll/json?t=k`
`localhost:5001/listAll/csv?t=k`
`localhost:5001/listOpenOnly/json?t=k`
`localhost:5001/listOpenOnly/csv?t=k`
`localhost:5001/listCloseOnly/json?t=k`
`localhost:5001/listCloseOnly/csv?t=k`
Will display "k" entries in from the database, if "k" is invalid list all.
## PHP Example Usage of the API - Consumer Program
`localhost:5002`
