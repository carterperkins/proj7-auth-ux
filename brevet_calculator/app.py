import os
from flask import Flask, redirect, url_for, request, render_template, flash, jsonify
import flask
from pymongo import MongoClient
import arrow
import acp_times
import urllib.parse
import json


from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

import auth_token
import auth_password

app = Flask(__name__)
app.config['SECRET_KEY'] = '123'
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
users = client.users
tokens = client.tokens

@app.route('/')
def todo(): 
    _items = db.tododb.find()
    items = [item for item in _items]
    print(items)
    return render_template('todo.html', items=items)
    
@app.route("/_clear", methods=['POST'])
def clear():
    db.tododb.drop()
    app.logger.debug("DROP TABLE: {}".format(db.tododb.count()))
    return redirect(url_for('todo'))

@app.route("/display", methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    app.logger.debug("DB: {}".format(items))

    return render_template('display.html', items=items) #header=header, items=items)

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request\n\n")
    app.logger.debug(request)
    app.logger.debug("\n\n")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', default=None, type=int)
    begin_date = request.args.get('begin_date', default=None, type=str)
    begin_time = request.args.get('begin_time', default=None, type=str)
    start_time = arrow.get('{} {}'.format(begin_date, begin_time)).replace(tzinfo='US/Pacific').isoformat()
    
    app.logger.debug("km={} distance={}, begin_date={}, begin_time={}, start_time={}".format(km, distance, begin_date, begin_time, start_time))
    app.logger.debug("request.args: {}".format(request.args))
    
    open_time = acp_times.open_time(km, distance, start_time)
    close_time = acp_times.close_time(km, distance, start_time)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/submit', methods=['POST', 'GET'])
def submit():
    
    if(db.tododb.count() != 0): # database isnt empty
        db.tododb.drop() # clear entries 
  # app.logger.debug("\n\n\n\n\n")
    data = urllib.parse.unquote(str(request.args.get('db')))
    app.logger.debug("\n\n_SUBMIT: {}\n\n".format(data))
   # app.logger.debug("data obj: {}".format( dataObj))
    src = u"[%s]" % data # format json properly
    data_dict = json.loads(src)
    for element in data_dict:
        app.logger.debug(element)
        db.tododb.insert_one(element)
    return redirect(url_for('todo'))
    
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    logout = SubmitField('Log Out (Clear Tokens)')

@app.route('/api/register', methods = ['POST', 'GET'])
def register():
    form = RegistrationForm()
    lform = LogoutForm()
    if lform.logout.data:
        app.logger.debug("TOKENS DROP")
        tokens.tokens.drop() # clear tokens on logout
    if form.validate_on_submit():
        app.logger.debug(form.username.data)
        app.logger.debug(form.password.data)
        if form.logout.data:
            tokens.tokens.drop() # clear tokens on logout
        hashed_password = auth_password.hash_password(form.password.data)
        _items = users.users.find()
        items = [item for item in _items]
        app.logger.debug(hashed_password)
        
        # check to be sure the username is unique
        for item in items:
            if item['username'] == form.username.data:
                return bad_request_error(Exception)

        # create id and json object
        index = 0
        app.logger.debug('count: {}'.format(users.users.count()))
        if users.users.count() == 0:
            index = 0
        else:
            index = int(items[-1]['id']) + 1
        app.logger.debug('id: {}'.format(index))
        
        account = {
            "id": index,
            "username": form.username.data,
            "password": hashed_password
        }

        users.users.insert_one(account)
        
        _items = users.users.find()
        items = [item for item in _items]
        
        app.logger.debug(items)
        response = flask.jsonify(username=form.username.data)
        
        response.headers['location'] = '/users/1'
        response.headers['content-type'] = 'application/json'
        response.autocorrect_location_header = False
        app.logger.debug(response.headers)
        return response, 201

    return render_template('register.html', form=form, lform=lform), 201


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')

class LogoutForm(FlaskForm):
    logout = SubmitField('Log Out (Clear Tokens)')

@app.route('/api/token', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    lform = LogoutForm()
    if lform.logout.data:
        app.logger.debug("TOKENS DROP")
        tokens.tokens.drop() # clear tokens on logout
    if form.validate_on_submit():
        app.logger.debug(form.username.data)
        app.logger.debug(form.password.data)
        app.logger.debug(form.remember.data)
        
        # hash password

        _items = users.users.find()
        items = [item for item in _items]
        for item in items:
            if item['username'] == form.username.data:
                hashed_psw = item['password']
                if auth_password.verify_password(form.password.data, hashed_psw):
                    # generate token and upload it to base
                    duration = 10
                    if form.remember.data:
                        duration = 9999
                    token = auth_token.generate_auth_token(duration)
                    token_json = {
                        "token": token,
                    }
                    tokens.tokens.insert_one(token_json)
                    app.logger.debug(token)


                    response = flask.jsonify(token=token.decode('utf-8'), duration=duration)

                    flask.flash('fornoefsija')
                    #response.headers['location'] = '/users/1'
                    #response.headers['content-type'] = 'application/json'
                    #response.autocorrect_location_header = False
                    
                    return response, 201
        return unauthorized_error(Exception)
    return render_template('login.html', form=form, lform=lform), 201


def verify_token():
    _items = tokens.tokens.find()
    items = [item for item in _items]
    app.logger.debug("TOKENS _: {}".format(items))
    validity = False
    app.logger.debug("COUNT: {}".format(len(items)))
    if len(items) == 0:
        validity = False
    elif len(items) == 1:
        validity = auth_token.verify_auth_token(items[0]["token"])
    else:
        validity = auth_token.verify_auth_token(items[-1]["token"])
    app.logger.debug("AUTH: {}".format(validity))
    if validity: # if "Success" then True, else if None then False
        validity = True
    else:
        validity = False
    return validity

@app.errorhandler(404)
def not_found_error(e):
    return render_template('404.html'), 404
    
@app.errorhandler(403)
def forbidden_error(e):
    return render_template('403.html'), 403

@app.errorhandler(401)
def unauthorized_error(e):
    return render_template('401.html'), 401

@app.errorhandler(400)
def bad_request_error(e):
    return render_template('400.html'), 400

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80,debug=True)
